yarn add express
npm install @types/express -D
yarn add typescript -D
// gera o arquivo tsconfig.json
yarn tsc --init
// gera os arquivos js
yarn tsc
// funciona igual o nodemom
yarn add ts-node-dev -D

yarn add eslint -D
//ajuda na importação dos arquivos
yarn add -D eslint-import-resolver-typescript
yarn add prettier eslint-config-prettier eslint-plugin-prettier  -D
yarn add uuidv4
yarn add date-fns
yarn add typeorm pg

// anotações
yarn add reflect-metadata
yarn add bcryptjs
yarn add install @types/bcryptjs
